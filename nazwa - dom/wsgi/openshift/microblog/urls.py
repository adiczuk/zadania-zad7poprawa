from django.conf.urls import patterns, url
from django.views.generic import ListView
import views

#from .models import Wpis

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^add$', views.dodaj_wpis, name='add'),
    url(r'^login$', views.zaloguj, name='login'),
    url(r'^logout$', views.wyloguj, name='logout'),
    url(r'^register$', views.rejestracja, name='register'),
    url(r'^mine$', views.moje_wpisy, name='mine'),
    url(r'^filtr$', views.filtruj, name='filtr'),
    url(r'^user/(?P<pk>\d+)$', views.wpisy, name='user'),
)