# -*- coding: utf-8 -*-
from django.test import TestCase
from django.test.client import Client
from django.test.utils import setup_test_environment
from models import Wpis


setup_test_environment()
client = Client()

class MicroblogTest(TestCase):

    def test_empty_db(self):
        assert Wpis.objects.count() == 0

    def test_200_Main_Page(self):
        response = self.client.get('')
        assert response.status_code == 200

    def test_302_Add_Page(self):
        response = self.client.get('/blog/add')
        assert response.status_code == 302

    def test_200_Register_Page(self):
        response = self.client.get('/blog/register')
        assert response.status_code == 200

    def test_200_Register_Page2(self):
        response = self.client.post('/blog/register', {'nick': 'adam', 'pass1': 'adam', 'pass2': 'adam'})
        assert response.status_code == 302

    def test_200_Login_Page_Correct_Pass(self):
        response = self.client.post('/blog/login', {'nick':'adam', 'password':'adam'})
        assert response.status_code == 302

    def test_200_Login_Page_Wrong_Pass(self):
        response = self.client.post('/blog/login', {'nick':'czacza', 'password':'czacza'})
        assert response.status_code == 302

    def test_200_Wrong_Page(self):
        response = self.client.post('/blog/dnscjkncknsdcjksd')
        assert response.status_code == 404