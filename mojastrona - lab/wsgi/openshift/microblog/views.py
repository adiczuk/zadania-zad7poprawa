# -*- coding: utf-8 -*-
from .models import Wpis
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


# Create your views here.

def index(request):
    return render_to_response('microblog/stub.html', {"wpisy": Wpis.objects.all().order_by('-pk')})


def dodaj_wpis(request):
    if request.method == 'POST':
        nick = request.POST['nick']
        tekst = request.POST['tekst']

        wpis = Wpis(nick=nick, text=tekst)
        wpis.save()
    return HttpResponseRedirect(reverse('index'))