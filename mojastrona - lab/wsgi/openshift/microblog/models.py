from django.db import models

# Create your models here.

class Wpis(models.Model):
    nick = models.CharField(max_length=20)
    text = models.TextField('Tekst wpisu')

    def __unicode__(self):
        return self.nick