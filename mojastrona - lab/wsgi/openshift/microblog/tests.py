from django.test import TestCase
from django.test.client import Client
from django.test.utils import setup_test_environment
from models import Wpis

setup_test_environment()
client = Client()

nick = "Nick"
text = "Tresc wpisu"

class MicroblogTest(TestCase):

    def DodajWpis(self):
        wpis = Wpis()
        wpis.nick = nick
        wpis.text = text
        wpis.save()

    def test_empty_db(self):
        assert Wpis.objects.count() == 0

    def test_add_one_post(self):
        self.DodajWpis()
        assert Wpis.objects.count() == 1

    def test_author(self):
        if Wpis.objects.count() == 0:
            self.DodajWpis()
            assert Wpis.objects.all()[0].nick == nick
            assert Wpis.objects.all()[0].text == text

    def test_main_page(self):
        response = client.get('/blog')
        print(response.content)
        assert True
